use arg_ripper::*;
use mockall::*;

struct Driver;

impl Driver {
    pub fn get_answer(&self) -> i32 {
        42
    }
}

mock! {
    Driver {
        pub fn get_answer(&self) -> i32;
    }
}

#[rip(driver: MockDriver)]
fn compute_answer() -> i32 {
    let driver = Driver;

    driver.get_answer()
}

#[test]
fn basic() {
    assert_eq!(compute_answer(), 42);

    let mut mock_driver = MockDriver::new();
    mock_driver.expect_get_answer().times(1).returning(|| 69);

    assert_eq!(ripped_compute_answer(mock_driver), 69);
}
