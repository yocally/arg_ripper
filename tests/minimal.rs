use arg_ripper::*;

#[rip(inner: i32)]
fn my_func() -> i32 {
    let inner = 42;
    inner
}

#[test]
fn my_func_normal_flow() {
    assert_eq!(my_func(), 42);
}

#[test]
fn my_func_ripped_test() {
    assert_eq!(ripped_my_func(69), 69);
}
