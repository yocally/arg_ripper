`arg_ripper` is a macro that allows for easy Dependency Injection for the purposes of mocking
internal variables in functions. It contains a single macro, rip that takes in a list of
local bindings (`let` statements) that occur within the annotated function, and generates a new
function with those as arguments instead of local bindings.

# Examples

```rust
#[rip(inner: i32)]
fn my_func() -> i32 {
    let inner = 42;
    inner
}

fn main() {
    assert_eq!(my_func(), 42);
    assert_eq!(ripped_my_func(69), 69);
}
```

The key feature that makes this useful for unit testing is the ability to change the type of
the argument you are ripping. Note the the type hint on line 1 of the example above, in this
case it's the same as the original version of `inner`, but it doesn't have to be. The only
restriction is that the new code must still compile, making things like this possible:

```rust
#[rip(answer: &str)]
fn print_answer() {
    let answer: i32 = 42;
    println!("{answer}");
}

fn main() {
    print_answer();
    ripped_print_answer("Fourty Two");
}
```

[The repository](https://gitlab.com/yocally/arg_ripper) has more in depth examples of how to use this with
[mockall](https://docs.rs/mockall/latest/mockall/) if you're
curious.
