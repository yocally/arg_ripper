//! `arg_ripper` is a macro that allows for easy Dependency Injection for the purposes of mocking
//! internal variables in functions. It contains a single macro, [macro@rip] that takes in a list of
//! local bindings (`let` statements) that occur within the annotated function, and generates a new
//! function with those as arguments instead of local bindings.
//!
//! # Examples
//!
//! ```rust
//! # #[macro_use] extern crate arg_ripper;
//! #[rip(inner: i32)]
//! fn my_func() -> i32 {
//!     let inner = 42;
//!     inner
//! }
//!
//! fn main() {
//!     assert_eq!(my_func(), 42);
//!     assert_eq!(ripped_my_func(69), 69);
//! }
//! ```
//!
//! The key feature that makes this useful for unit testing is the ability to change the type of
//! the argument you are ripping. Note the the type hint on line 1 of the example above, in this
//! case it's the same as the original version of `inner`, but it doesn't have to be. The only
//! restriction is that the new code must still compile, making things like this possible:
//!
//! ```rust
//! #[rip(answer: &str)]
//! fn print_answer() {
//!     let answer: i32 = 42;
//!     println!("{answer}");
//! }
//!
//! fn main() {
//!     print_answer();
//!     ripped_print_answer("Fourty Two");
//! }
//! ```
//!
//! [The repository](https://gitlab.com/yocally/arg_ripper) has more in depth examples of how to use this with
//! [mockall](https://docs.rs/mockall/latest/mockall/) if you're
//! curious.

use proc_macro::TokenStream;
use proc_macro2::Span;
use quote::quote;
use syn::{
    parse::{Parse, ParseStream},
    parse_macro_input,
    punctuated::Punctuated,
    token::Comma,
    Block, FnArg, Ident, ItemFn, Pat, Result, Stmt, Token,
};

struct Args {
    vars: Vec<FnArg>,
}

impl Parse for Args {
    fn parse(input: ParseStream) -> Result<Self> {
        let vars = Punctuated::<FnArg, Token![,]>::parse_terminated(input)?;
        Ok(Args {
            vars: vars.into_iter().collect(),
        })
    }
}

#[proc_macro_attribute]
pub fn rip(metadata: TokenStream, input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as ItemFn);
    let args = parse_macro_input!(metadata as Args);

    // Signature for the new function. Build the ident from the old one, prepended with 'ripped_'
    let mut new_sig = input.clone().sig;
    new_sig.ident = Ident::new(&format!("ripped_{}", input.sig.ident), Span::call_site());

    // Argument list we're going to append to the original for the new function
    let mut new_args: Vec<FnArg> = vec![];

    let old_block = input.clone().block;

    let mut new_block = Block {
        brace_token: old_block.brace_token,
        stmts: vec![],
    };

    for statement in old_block.stmts {
        if let Stmt::Local(local) = statement.clone() {
            match local.pat {
                Pat::Ident(ident) => {
                    let matching_arg = args.vars.iter().find(|e| match e {
                        FnArg::Typed(typed) => {
                            if let Pat::Ident(inner_ident) = *typed.pat.clone() {
                                ident.ident == inner_ident.ident
                            } else {
                                false
                            }
                        }
                        _ => false,
                    });

                    if let Some(e) = matching_arg {
                        new_args.push(e.clone());
                        continue;
                    }
                }
                Pat::Type(pat) => {
                    if let Pat::Ident(a) = *pat.pat {
                        // Note that this find doesn't pop the item, so we will replace all
                        // instances that match the input ident
                        let matching_arg = args.vars.iter().find(|e| match e {
                            FnArg::Typed(typed) => {
                                if let Pat::Ident(inner_ident) = *typed.pat.clone() {
                                    a.ident == inner_ident.ident
                                } else {
                                    false
                                }
                            }
                            _ => false,
                        });

                        if let Some(e) = matching_arg {
                            new_args.push(e.clone());
                            continue;
                        }
                        // This section once prevented multiple bindings with the same name
                        // (shadowing) from getting removed. Unfortunately, after a refactor, it
                        // stopped working.
                        // if args.vars.contains(&FnArg::Typed(a.ident)) {
                        //     new_args.push(FnArg::Typed(PatType {
                        //         attrs: vec![],
                        //         pat: Box::new(Pat::Ident(PatIdent {
                        //             attrs: vec![],
                        //             by_ref: None,
                        //             mutability: a.mutability,
                        //             ident: a.ident.clone(),
                        //             subpat: None,
                        //         })),
                        //         colon_token: Colon {
                        //             spans: [Span::call_site()],
                        //         },
                        //         ty: pat.ty,
                        //     }));
                        //     // Pop the ident that we found out of args. This prevents us from
                        //     // replacing ALL local bindings with a given ident, allowing you to
                        //     // shadow things that you ripped
                        //     args.vars.remove(&a.ident);
                        //     continue;
                        // }
                    }
                }
                _ => {}
            }
        }

        // If the statement wasn't a local binding (let block) that matched what we're looking for,
        // push the statement to the new statement list
        // This effectively lets us retain all the statements of the original function, skipping
        // local bindings that we want to 'rip' out into arguments
        new_block.stmts.push(statement);
    }

    // Merge the new arguments into the old ones
    for i in new_args.clone() {
        // Push a trailing comma if there isn't one
        if !new_sig.inputs.trailing_punct() && !new_sig.inputs.is_empty() {
            new_sig.inputs.push_punct(Comma {
                spans: [Span::call_site()],
            });
        }

        new_sig.inputs.push_value(i.clone());
    }

    TokenStream::from(quote! {
        #new_sig #new_block

        #input
    })
}
