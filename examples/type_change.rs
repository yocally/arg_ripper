#[macro_use]
extern crate arg_ripper;

#[rip(answer: &str)]
fn print_answer() {
    let answer: i32 = 42;
    println!("{answer}");
}

fn main() {
    print_answer();
    ripped_print_answer("Fourty Two");
}
