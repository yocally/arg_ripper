#[arg_ripper::rip(inner: i32)]
fn my_func() -> i32 {
    let inner = 42;
    inner
}

fn main() {
    assert_eq!(my_func(), 42);
    assert_eq!(ripped_my_func(69), 69);
}
