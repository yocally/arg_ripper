use mockall::mock;

struct Driver;

impl Driver {
    pub fn get_answer(&self) -> i32 {
        42
    }
}

mock! {
    Driver {
        pub fn get_answer(&self) -> i32;
    }
}

#[arg_ripper::rip(driver: MockDriver)]
fn compute_answer() -> i32 {
    let driver = Driver;
    driver.get_answer()
}

fn main() {
    assert_eq!(compute_answer(), 42);

    let mut mock_driver = MockDriver::new();
    mock_driver.expect_get_answer().return_once(|| 69);

    assert_eq!(69, ripped_compute_answer(mock_driver));
}
